import { useState } from 'react';
import './App.css';
import DateDisplay from './components/DatesSelect/DateDisplay';
import DateSelectMenu from './components/DatesSelect/DateSelectMenu';
import DegreePick from './components/DegreePick';
import LocationInput from './components/LocationInput';
import WeatherChart from './components/WeatherChart';
import WeatherSlider from './components/WeatherSlider';
import { useWeatherForecast } from './context/weather-forecast.context';

function App() {
  const { location, error } = useWeatherForecast();
  const [displayDateSelectMenu, setDisplayDateChangeMenu] = useState<
    'end' | 'start' | null
  >(null);
  return (
    <div className="py-8 px-4 flex flex-col gap-4 items-center">
      <div className="flex flex-col items-center gap-2 w-full">
        <DegreePick />
        <LocationInput />
        <div className="relative">
          <DateDisplay
            onRequestToChangeDate={(field) => setDisplayDateChangeMenu(field)}
          />
          {displayDateSelectMenu && (
            <div className="absolute top-10 z-50 border border-slate-300 rounded-lg shadow-lg left-1/2 -translate-x-1/2">
              <DateSelectMenu
                dateField={displayDateSelectMenu}
                onCancel={() => setDisplayDateChangeMenu(null)}
              />
            </div>
          )}
        </div>
      </div>
      {!error ? (
        <>
          <h1 className="text-4xl font-bold mb-8 text-center">
            Weather Forecast in {location}
          </h1>
          <WeatherSlider />
          <WeatherChart />
        </>
      ) : (
        <h3 className="text-xl font-bold text-red-500 mb-8 text-center">
          {error}
        </h3>
      )}
    </div>
  );
}

export default App;
