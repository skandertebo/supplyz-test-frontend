import { Button, Input } from 'antd';
import { useState } from 'react';
import { useWeatherForecast } from '../../context/weather-forecast.context';

const LocationInput: React.FC = () => {
  const { mutateLocation } = useWeatherForecast();
  const [inputVal, setInputVal] = useState('');

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setInputVal(e.target.value);

  const handleInputSubmit = () => {
    mutateLocation(inputVal);
    const searchParams = new URLSearchParams(window.location.search);
    searchParams.set('location', inputVal);
    window.history.pushState(null, '', `?${searchParams.toString()}`);
  };

  return (
    <div className="flex gap-2 items-center w-[60%]">
      <Input
        type="text"
        value={inputVal}
        onChange={handleInputChange}
        placeholder="Search by City"
      />
      <Button color="light-blue" onClick={handleInputSubmit}>
        Search
      </Button>
    </div>
  );
};

export default LocationInput;
