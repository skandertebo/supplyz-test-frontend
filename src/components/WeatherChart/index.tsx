import { Column } from '@ant-design/charts';
import React, { useEffect, useRef, useState } from 'react';
import {
  TemperatureUnit,
  useWeatherForecast
} from '../../context/weather-forecast.context';
import useWindowViewport from '../../hooks/useWindowViewport';
import convertCelsiusToFahrenheit from '../../utils/celsius-to-fahrenheit.utils';
const WeatherChart: React.FC = () => {
  const { weatherForecast, loading, unit } = useWeatherForecast();
  const { width } = useWindowViewport();
  const shouldAnimate = useRef<boolean>(false);
  const data = weatherForecast
    ? weatherForecast?.map((weatherData, idx) => ({
        day: new Date(weatherData.time).toLocaleDateString(),
        temperature:
          unit === TemperatureUnit.CELSIUS
            ? weatherData.values.temperatureAvg
            : convertCelsiusToFahrenheit(weatherData.values.temperatureAvg)
      }))
    : null;

  // for some reason the chart does not resize when updating state, here is a little workaround
  const [stopRender, setStopRender] = useState(false);

  const config = {
    animate: shouldAnimate.current,
    data,
    width: width < 400 ? width - 60 : width < 600 ? 400 : 590,
    height: 200,
    xField: 'day',
    yField: 'temperature',
    spacing: 10,
    lineWidth: 10
  };
  useEffect(() => {
    setStopRender(true);
    shouldAnimate.current = false;
    setTimeout(() => {
      setStopRender(false);
    }, 0);
  }, [width]);

  return weatherForecast && !stopRender ? <Column {...config} /> : null;
};
export default WeatherChart;
