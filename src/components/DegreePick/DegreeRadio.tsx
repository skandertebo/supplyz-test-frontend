import { Radio, RadioChangeEvent } from 'antd';
import { HTMLAttributes } from 'react';
export type DegreePickProps = HTMLAttributes<HTMLInputElement> & {
  checked: boolean;
  label: string;
  onChange: (e: RadioChangeEvent) => void;
};

const DegreeRadio: React.FC<DegreePickProps> = ({ label, ...props }) => {
  return (
    <div className="flex items-center gap-2">
      <Radio {...props} type="radio" name="unit" />
      {label}
    </div>
  );
};

export default DegreeRadio;
