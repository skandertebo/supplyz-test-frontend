import {
  TemperatureUnit,
  useWeatherForecast
} from '../../context/weather-forecast.context';
import DegreeRadio from './DegreeRadio';

const DegreePick = () => {
  const { unit, mutateUnit } = useWeatherForecast();
  return (
    <div className="w-full flex justify-center gap-8">
      <DegreeRadio
        checked={unit === TemperatureUnit.CELSIUS}
        onChange={() => mutateUnit(TemperatureUnit.CELSIUS)}
        label={'Celcius'}
      />
      <DegreeRadio
        checked={unit === TemperatureUnit.FAHRENHEIT}
        onChange={() => mutateUnit(TemperatureUnit.FAHRENHEIT)}
        label={'Fahrenheit'}
      />
    </div>
  );
};

export default DegreePick;
