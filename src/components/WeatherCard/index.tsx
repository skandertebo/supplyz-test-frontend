import {
  TemperatureUnit,
  useWeatherForecast
} from '../../context/weather-forecast.context';
import { WeatherData } from '../../types/weather.types';
import convertCelsiusToFahrenheit from '../../utils/celsius-to-fahrenheit.utils';

export interface WeatherCardProps {
  weatherForecast: WeatherData;
  time?: string;
}

const WeatherCard: React.FC<WeatherCardProps> = ({ weatherForecast, time }) => {
  const { unit } = useWeatherForecast();
  return (
    <div className="flex flex-col items-center justify-center w-64 h-64 bg-gray-100 rounded-md shadow-lg weather-card">
      <h1 className="text-2xl">
        {unit === TemperatureUnit.CELSIUS
          ? weatherForecast.temperatureAvg + '°C'
          : convertCelsiusToFahrenheit(weatherForecast.temperatureAvg) + '°F'}
      </h1>
      <h2 className="text-xl font-bold">
        {time ? new Date(time).toDateString() : null}
      </h2>
    </div>
  );
};

export default WeatherCard;
