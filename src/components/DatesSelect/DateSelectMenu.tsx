import { useState } from 'react';
import { useWeatherForecast } from '../../context/weather-forecast.context';

export interface DateSelectProps {
  dateField: 'start' | 'end';
  onCancel: () => void;
}

const DateSelectMenu: React.FC<DateSelectProps> = ({ dateField, onCancel }) => {
  const { startDate, endDate, setStartDate, setEndDate } = useWeatherForecast();
  const [date, setDate] = useState<string>(
    dateField === 'start' ? startDate : endDate
  );
  const [error, setError] = useState<string | null>(null);
  const onDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    const date = new Date(value);
    if (date.toString() === 'Invalid Date') {
      setError('Invalid Date');
      return;
    }
    setError(null);
    setDate(value);
  };

  const today = new Date();
  today.setUTCHours(0, 0, 0, 0);

  const fiveDaysFromNow = new Date();
  fiveDaysFromNow.setUTCHours(0, 0, 0, 0);
  fiveDaysFromNow.setDate(fiveDaysFromNow.getDate() + 4);

  const onDateSubmit = () => {
    if (error) return;
    if (dateField === 'start') {
      setStartDate(date);
    } else {
      setEndDate(date);
    }
    onCancel();
  };
  return (
    <div className="bg-white rounded-lg p-8 flex flex-col gap-4">
      <h3 className="text-2xl font-bold text-center">
        {dateField === 'start' ? 'Start Date' : 'End Date'}
      </h3>
      <div className="flex flex-col gap-2">
        <label htmlFor="date">Date</label>
        <input
          type="date"
          id="date"
          className="border border-gray-200 rounded-lg p-2"
          value={date}
          onChange={onDateChange}
          max={
            dateField === 'start'
              ? endDate.split('T')[0]
              : fiveDaysFromNow.toISOString().split('T')[0]
          }
          min={
            dateField === 'start'
              ? today.toISOString().split('T')[0]
              : startDate.split('T')[0]
          }
        />
        {error && <p className="text-red-500 text-sm font-bold">{error}</p>}
      </div>
      <div className="flex gap-2 justify-end">
        <button
          className="bg-sky-600 text-white px-4 py-2 rounded-lg"
          onClick={onDateSubmit}
        >
          Submit
        </button>
        <button
          className="bg-red-600 text-white px-4 py-2 rounded-lg"
          onClick={onCancel}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default DateSelectMenu;
