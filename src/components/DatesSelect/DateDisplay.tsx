import { useWeatherForecast } from '../../context/weather-forecast.context';

export interface DateDisplayProps {
  onRequestToChangeDate: (field: 'start' | 'end') => void;
}

const DateDisplay: React.FC<DateDisplayProps> = ({ onRequestToChangeDate }) => {
  const { startDate, endDate } = useWeatherForecast();
  return (
    <div className="flex gap-x-6 gap-y-1 flex-wrap justify-center">
      <div className="flex gap-2 items-center py-1 px-2 border border-gray-200 rounded-lg">
        <strong>Start Date:</strong>
        <p>{new Date(startDate).toDateString()}</p>
        <button
          className="text-sky-600 underline"
          onClick={() => onRequestToChangeDate('start')}
        >
          Change
        </button>
      </div>
      <div className="flex gap-2 items-center py-1 px-2 border border-gray-200 rounded-lg">
        <strong>End Date:</strong>
        <p>{new Date(endDate).toDateString()}</p>
        <button
          className="text-sky-600 underline"
          onClick={() => onRequestToChangeDate('end')}
        >
          Change
        </button>
      </div>
    </div>
  );
};

export default DateDisplay;
