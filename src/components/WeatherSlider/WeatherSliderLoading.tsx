import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const WeatherSliderLoading: React.FC = () => {
  return (
    <div className="h-[270px] flex justify-center items-center">
      <Spin indicator={antIcon} />
    </div>
  );
};

export default WeatherSliderLoading;
