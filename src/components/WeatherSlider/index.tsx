import { useEffect, useState } from 'react';
import { useWeatherForecast } from '../../context/weather-forecast.context';
import { clsxm } from '../../utils/clsxm.utils';
import WeatherCard from '../WeatherCard';
import WeatherSliderLoading from './WeatherSliderLoading';

const CARD_DIMENSION = 256;

const WeatherSlider: React.FC = () => {
  const { weatherForecast, loading } = useWeatherForecast();
  const [currentSlide, setCurrentSlide] = useState<number>(0);

  const handleNextSlide = () => {
    setCurrentSlide((prev) => prev + 1);
  };

  const handlePrevSlide = () => {
    setCurrentSlide((prev) => prev - 1);
  };

  useEffect(() => {
    setCurrentSlide(0);
  }, [weatherForecast]);

  if (loading) return <WeatherSliderLoading />;
  if (weatherForecast.length === 0)
    return (
      <h3 className="text-xl font-bold text-red-500 mb-8 text-center">
        No data found
      </h3>
    );
  return (
    <div
      style={{
        width: `${CARD_DIMENSION * 3 + 90}px`
      }}
      className="relative"
    >
      <div
        className={clsxm(
          'overflow-x-hidden relative transition-all duration-300 h-[270px]'
        )}
      >
        {weatherForecast?.map((weatherData, idx) => (
          <div
            className={clsxm(
              'flex-1 absolute transition-all duration-300',
              currentSlide === idx
                ? 'left-1/2 translate-x-[-50%]'
                : idx === currentSlide - 1
                ? 'left-0'
                : idx === currentSlide + 1
                ? 'left-full translate-x-[-100%]'
                : currentSlide > idx
                ? '-left-64'
                : '-right-64',
              currentSlide === idx
                ? 'scale-105 opacity-100'
                : 'opacity-0 md:opacity-70'
            )}
            key={idx}
          >
            <WeatherCard
              key={idx}
              weatherForecast={weatherData.values}
              time={weatherData.time}
            />
          </div>
        ))}
      </div>
      <button
        className={clsxm(
          'absolute left-[30%] md:left-0 z-10 p-2 rounded-full bg-white border border-gray-400 shadow-lg top-1/2 -translate-y-1/2',
          currentSlide === 0 ? 'hidden' : 'block'
        )}
        onClick={handlePrevSlide}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-6 h-6 text-gray-700"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path d="M15 19l-7-7 7-7" />
        </svg>
      </button>
      <button
        className={clsxm(
          'absolute right-[30%] md:right-0 z-10 p-2 rounded-full bg-white border border-gray-400 top-1/2 -translate-y-1/2 shadow-lg',
          currentSlide === weatherForecast?.length! - 1 ? 'hidden' : 'block'
        )}
        onClick={handleNextSlide}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="w-6 h-6 text-gray-700"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path d="M9 5l7 7-7 7" />
        </svg>
      </button>
    </div>
  );
};

export default WeatherSlider;
