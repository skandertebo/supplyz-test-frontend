import { AxiosError } from 'axios';
import { createContext, useContext, useEffect, useState } from 'react';
import WeatherForecastService from '../services/WeatherForecast/weather-forecast.service';
import { WeatherApiResponse } from '../types/weather.types';

export enum TemperatureUnit {
  CELSIUS = 'C',
  FAHRENHEIT = 'F'
}

export interface WeatherForecastContext {
  weatherForecast: WeatherApiResponse | null;
  loading: boolean;
  error: string | null;
  location: string;
  mutateLocation: (val: string) => void;
  unit: TemperatureUnit;
  mutateUnit: (val: TemperatureUnit) => void;
  startDate: string;
  endDate: string;
  setStartDate: (val: string) => void;
  setEndDate: (val: string) => void;
}

const weatherForecastContext = createContext<WeatherForecastContext>(
  {} as WeatherForecastContext
);

export const useWeatherForecast = () => useContext(weatherForecastContext);

export const WeatherForecastProvider: React.FC<React.PropsWithChildren> = ({
  children
}) => {
  const [weatherForecast, setWeatherForecast] =
    useState<WeatherApiResponse | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [location, setLocation] = useState<string>(() => {
    const searchParams = new URLSearchParams(window.location.search);
    const location = searchParams.get('location');
    return location || 'tunis';
  });
  const [unit, setUnit] = useState<TemperatureUnit>(TemperatureUnit.CELSIUS);
  const [startDate, setStartDate] = useState<string>(() => {
    const today = new Date();
    today.setUTCHours(0, 0, 0, 0);
    return today.toISOString();
  });
  const [endDate, setEndDate] = useState<string>(() => {
    const fiveDaysFromNow = new Date();
    fiveDaysFromNow.setDate(fiveDaysFromNow.getDate() + 4);
    fiveDaysFromNow.setUTCHours(0, 0, 0, 0);
    return fiveDaysFromNow.toISOString();
  });
  const mutateUnit = (val: TemperatureUnit) => {
    if (val !== TemperatureUnit.CELSIUS && val !== TemperatureUnit.FAHRENHEIT)
      throw new Error('Invalid unit');
    setUnit(val);
  };
  useEffect(() => {
    (async () => {
      try {
        setError(null);
        setLoading(true);
        const res = await WeatherForecastService.getWeatherForecast(
          location,
          startDate,
          endDate
        );
        setWeatherForecast(res);
      } catch (err) {
        if (err instanceof AxiosError) setError(err.response?.data.message);
        else if (err instanceof Error) setError(err.message);
        else setError('Something went wrong!');
      } finally {
        setLoading(false);
      }
    })();
  }, [location, startDate, endDate]);

  const mutateLocation = (val: string) => setLocation(val);

  return (
    <weatherForecastContext.Provider
      value={{
        weatherForecast,
        loading,
        error,
        location,
        mutateLocation,
        unit,
        mutateUnit,
        startDate,
        endDate,
        setStartDate,
        setEndDate
      }}
    >
      {children}
    </weatherForecastContext.Provider>
  );
};
