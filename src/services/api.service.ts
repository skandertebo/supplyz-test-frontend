import axios from 'axios';
import { weatherApiBaseUrl } from '../constants';

const apiService = axios.create({
  baseURL: weatherApiBaseUrl
});

export default apiService;
