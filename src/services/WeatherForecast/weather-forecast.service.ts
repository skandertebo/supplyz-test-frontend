import { WeatherApiResponse } from '../../types/weather.types';
import apiService from '../api.service';

export default class WeatherForecastService {
  static async getWeatherForecast(
    city: string,
    startDate: string,
    endDate: string
  ) {
    const response = await apiService.get<WeatherApiResponse>('forecast', {
      params: {
        location: city,
        startDate: startDate,
        endDate: endDate
      }
    });
    return response.data;
  }
}
