export default function convertCelsiusToFahrenheit(celsius: number): string {
  return ((celsius * 9) / 5 + 32).toFixed(2);
}
